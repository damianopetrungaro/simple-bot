<?php

namespace App\Plugins;


class PluginHandler
{
    /**
     * @var PluggableInterface[]
     */
    private $plugins = [];

    public function __construct(array $plugins)
    {
        foreach ($plugins as $plugin) {
            if (!$plugin instanceof PluggableInterface) {
                throw new \InvalidArgumentException("All plugin must implement PluginStrategyInterface");
            }
            $this->plugins[] = $plugin;
        }
    }

    /**
     * Return a matching plugin
     *
     * @param $message
     *
     * @return PluggableInterface|null
     */
    public function getMatchingPlugin($message)
    {
        foreach ($this->plugins as $plugin) {
            if ($plugin->match($message)) {
                return $plugin;
            }
        }

        return null;
    }
}