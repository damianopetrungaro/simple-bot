<?php

namespace App\Plugins;

interface PluggableInterface
{
    /**
     *
     *
     * @param $message
     *
     * @return string
     */
    public function trigger($message);

    /**
     *
     *
     * @param $message
     *
     * @return bool
     */
    public function match($message);
}