<?php

namespace App\Plugins\Barzelletta;

use App\Plugins\Barzelletta\Model\BarzellettaModel;
use App\Plugins\Barzelletta\Repositories\BarzellettaRepositoryInterface;
use App\Plugins\PluggableInterface;

class AddBarzelletta implements PluggableInterface
{
    private $regex = '#^@alfred barzelletta "([^"]+)" "([^"]+)"$#';

    /**
     * @var BarzellettaRepositoryInterface
     */
    private $barzellettaRepository;

    /**
     * AddBarzelletta constructor.
     * @param BarzellettaRepositoryInterface $barzellettaRepository
     */
    public function __construct(BarzellettaRepositoryInterface $barzellettaRepository)
    {
        $this->barzellettaRepository = $barzellettaRepository;
    }

    public function trigger($message)
    {
        preg_match($this->regex, $message, $matches);
        $barzelletta = new BarzellettaModel();
        $barzelletta->createFromContentAndAuthor($this->barzellettaRepository->nextId(), $matches[1], $matches[2]);
        $this->barzellettaRepository->add($barzelletta);

        return "Barzelletta Aggiunta";
    }

    public function match($message)
    {
        return (bool)preg_match($this->regex, $message);
    }
}