<?php

namespace App\Plugins\Barzelletta\Model;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class BarzellettaModel extends Model
{
    protected $fillable = ['id', 'content', 'created_by', 'last_said_at'];
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $table = 'barzellette';

    public function createFromContentAndAuthor(UuidInterface $id, $content, $author)
    {
        $this->id = $id;
        $this->content = $content;
        $this->author = $author;
        $this->setAsLastSaid();
    }

    /**
     * Create a BarzellettaModel from params
     * This could be inserted into a factory
     *
     * @param array $params
     *
     * @return BarzellettaModel
     */
    public static function createFromParams(array $params)
    {
        $barzelletta = new self();
        $barzelletta->id = Uuid::fromString($params[0]);
        $barzelletta->content = $params[1];
        $barzelletta->author = $params[2];
        $barzelletta->last_said_at = new \DateTimeImmutable($params[3]['date']);

        return $barzelletta;
    }

    public function setAsLastSaid()
    {
        $now = new \DateTimeImmutable();
        $this->last_said_at = $now;
    }
}
