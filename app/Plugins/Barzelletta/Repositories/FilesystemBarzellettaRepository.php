<?php

namespace App\Plugins\Barzelletta\Repositories;


use App\Plugins\Barzelletta\Model\BarzellettaModel;
use App\Plugins\Barzelletta\Repositories\Exceptions\BarzellettaNotFoundException;
use App\Plugins\Barzelletta\Repositories\Exceptions\BarzellettaPersistenceException;
use Illuminate\Filesystem\Filesystem;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class FilesystemBarzellettaRepository implements BarzellettaRepositoryInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var
     */
    private $mainDirectory;

    /**
     * FilesystemBarzellettaRepository constructor.
     * @param Filesystem $filesystem
     * @param $mainDirectory
     */
    public function __construct(Filesystem $filesystem, $mainDirectory)
    {
        $this->filesystem = $filesystem;
        $this->mainDirectory = $mainDirectory;
    }

    /**
     * Add a new Barzelletta
     *
     * @param BarzellettaModel $barzelletta
     *
     * @return bool
     *
     * @throws BarzellettaPersistenceException
     */
    public function add(BarzellettaModel $barzelletta)
    {
        try {
            $barzelletta = $barzelletta->toArray();
            $data = $this->generateDaraFromBarzelletta($barzelletta);
            return (bool)$this->addFileToFilesystem($barzelletta, $data);
        } catch (\Exception $e) {
            throw new BarzellettaPersistenceException($e->getMessage());
        }
    }

    /**
     * Edit barzelletta
     *
     * @param BarzellettaModel $barzelletta
     *
     * @return bool
     *
     * @throws BarzellettaPersistenceException
     */
    public function edit(BarzellettaModel $barzelletta)
    {
        try {
            $barzelletta = $barzelletta->toArray();
            $data = $this->generateDaraFromBarzelletta($barzelletta);
            $path = $this->filesystem->glob("{$this->mainDirectory}/*{$barzelletta['id']->__toString()}.txt")[0];
            // Add to filesystem and delete old file
            $this->addFileToFilesystem($barzelletta, $data);
            $this->filesystem->delete($path);
        } catch (\Exception $e) {
            throw new BarzellettaPersistenceException($e->getMessage());
        }
    }

    /**
     * Get a Barzelletta said a long time ago
     *
     * @return BarzellettaModel
     *
     * @throws BarzellettaNotFoundException
     * @throws BarzellettaPersistenceException
     */
    public function getOneSaidLongAgo()
    {
        try {
            $files = $this->filesystem->glob("{$this->mainDirectory}/*.txt");
        } catch (\Exception $e) {
            throw new BarzellettaPersistenceException($e->getMessage());
        }

        if (empty($files) || count($files) <= 0) {
            throw new BarzellettaNotFoundException();
        }

        $barzellettaData = json_decode(file_get_contents($files[0]), true);
        return BarzellettaModel::createFromParams($barzellettaData);
    }

    /**
     * Return next id
     *
     * @return UuidInterface
     */
    public function nextId()
    {
        return Uuid::uuid1();
    }

    /**
     * Save a file to filesystem
     *
     * @param array $barzelletta
     * @param $data
     *
     * @return int
     */
    private function addFileToFilesystem($barzelletta, $data)
    {
        // Used only as helper for getOneSaidLongAgo
        /** @var \DateTimeImmutable $lastSaidAt */
        $timestamp = $barzelletta['last_said_at']->format('Y_m_d_H_i_s');
        return $this->filesystem->put("{$this->mainDirectory}/{$timestamp}-{$barzelletta['id']->__toString()}.txt", $data);
    }

    /**
     * Generate data to insert into the file
     *
     * @param array $barzelletta
     *
     * @return string
     */
    private function generateDaraFromBarzelletta(array $barzelletta)
    {
        return json_encode([$barzelletta['id'], $barzelletta['content'], $barzelletta['author'], $barzelletta['last_said_at']]);
    }
}