<?php

namespace App\Plugins\Barzelletta\Repositories\Exceptions;


class BarzellettaPersistenceException extends \Exception
{
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}