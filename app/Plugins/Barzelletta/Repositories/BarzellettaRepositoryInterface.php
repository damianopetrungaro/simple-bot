<?php

namespace App\Plugins\Barzelletta\Repositories;


use App\Plugins\Barzelletta\Model\BarzellettaModel;
use App\Plugins\Barzelletta\Repositories\Exceptions\BarzellettaNotFoundException;
use App\Plugins\Barzelletta\Repositories\Exceptions\BarzellettaPersistenceException;
use Ramsey\Uuid\UuidInterface;

interface BarzellettaRepositoryInterface
{

    /**
     * /**
     * Add a new Barzelletta
     *
     * @param BarzellettaModel $barzelletta
     *
     * @return bool
     *
     * @throws BarzellettaPersistenceException
     */
    public function add(BarzellettaModel $barzelletta);

    /**
     * Edit barzelletta
     *
     * @param BarzellettaModel $barzelletta
     *
     * @return bool
     *
     * @throws BarzellettaPersistenceException
     */
    public function edit(BarzellettaModel $barzelletta);

    /**
     * Get a Barzelletta said a long time ago
     *
     * @return BarzellettaModel
     *
     * @throws BarzellettaNotFoundException
     * @throws BarzellettaPersistenceException
     */
    public function getOneSaidLongAgo();

    /**
     * Return next id
     *
     * @return UuidInterface
     */
    public function nextId();
}