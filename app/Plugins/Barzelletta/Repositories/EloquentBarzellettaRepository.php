<?php

namespace App\Plugins\Barzelletta\Repositories;


use App\Plugins\Barzelletta\Model\BarzellettaModel;
use App\Plugins\Barzelletta\Repositories\Exceptions\BarzellettaNotFoundException;
use App\Plugins\Barzelletta\Repositories\Exceptions\BarzellettaPersistenceException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class EloquentBarzellettaRepository implements BarzellettaRepositoryInterface
{
    /**
     * Add a new Barzelletta
     *
     * @param BarzellettaModel $barzelletta
     *
     * @return bool
     *
     * @throws BarzellettaPersistenceException
     */
    public function add(BarzellettaModel $barzelletta)
    {
        try {
            $barzelletta->save();
        } catch (\Exception $e) {
            throw new BarzellettaPersistenceException($e->getMessage());
        }
    }

    /**
     * Edit barzelletta
     *
     * @param BarzellettaModel $barzelletta
     *
     * @return bool
     *
     * @throws BarzellettaPersistenceException
     */
    public function edit(BarzellettaModel $barzelletta)
    {
        try {
            $barzelletta->update();
        } catch (\Exception $e) {
            throw new BarzellettaPersistenceException($e->getMessage());
        }
    }

    /**
     * Get a Barzelletta said a long time ago
     *
     * @return BarzellettaModel
     *
     * @throws BarzellettaNotFoundException
     * @throws BarzellettaPersistenceException
     */
    public function getOneSaidLongAgo()
    {
        try {
            $barzelletta = BarzellettaModel::orderBy('last_said_at', 'asc')->first();
        } catch (\Exception $e) {
            throw new BarzellettaPersistenceException($e->getMessage());
        }

        if (!$barzelletta) {
            throw new BarzellettaNotFoundException();
        }

        return $barzelletta;
    }

    /**
     * Return next id
     *
     * @return UuidInterface
     */
    public function nextId()
    {
        return Uuid::uuid1();
    }
}