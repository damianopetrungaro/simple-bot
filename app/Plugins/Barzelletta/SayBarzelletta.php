<?php

namespace App\Plugins\Barzelletta;

use App\Plugins\Barzelletta\Model\BarzellettaModel;
use App\Plugins\Barzelletta\Repositories\BarzellettaRepositoryInterface;
use App\Plugins\PluggableInterface;

class SayBarzelletta implements PluggableInterface
{

    /**
     * @var BarzellettaRepositoryInterface
     */
    private $barzellettaRepository;

    /**
     * AddBarzelletta constructor.
     * @param BarzellettaRepositoryInterface $barzellettaRepository
     */
    public function __construct(BarzellettaRepositoryInterface $barzellettaRepository)
    {
        $this->barzellettaRepository = $barzellettaRepository;
    }

    public function trigger($message)
    {
        $barzelletta = $this->barzellettaRepository->getOneSaidLongAgo();
        $barzelletta->setAsLastSaid();
        $this->barzellettaRepository->edit($barzelletta);

        return $barzelletta->content;
    }

    public function match($message)
    {
        return (bool)preg_match('#^@alfred barzelletta$#', $message);
    }
}