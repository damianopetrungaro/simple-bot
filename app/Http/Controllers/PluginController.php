<?php

namespace App\Http\Controllers;

use App\Plugins\PluggableInterface;
use App\Plugins\PluginHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;

class PluginController extends Controller
{
    /**
     * @var PluginHandler
     */
    private $pluginHandler;

    /**
     * @var Response
     */
    private $response;

    /**
     * PluginController constructor.
     * @param Response $response
     * @param PluginHandler $pluginHandler
     */
    public function __construct(Response $response, PluginHandler $pluginHandler)
    {
        $this->response = $response;
        $this->pluginHandler = $pluginHandler;
    }

    /**
     *
     * @param Request $request
     *
     * @return Response
     */
    public function handle(Request $request)
    {
        // Create Validator for plugin requests
        $validator = Validator::make($request->all(), [
            'password' => 'required|in:12345',
            'message' => 'required|regex:/^@alfred \w+/'
        ]);

        // If validation fails return a 404 status code with an empty body
        if ($validator->fails()) {
            return response('', 404);
        }

        // Try to find a matching plugin
        $message = $request->get('message');
        $plugin = $this->pluginHandler->getMatchingPlugin($message);

        // If a matching plugin exists trigger it and return
        // If an exception is thrown, return a 500
        if ($plugin instanceof PluggableInterface) {
            return response($plugin->trigger($message), 200);
        }

        // If no plugin were found return a 404
        return response('', 404);
    }
}
