<?php

namespace App\Providers;

use App\Plugins\Barzelletta\Repositories\BarzellettaRepositoryInterface;
use App\Plugins\Barzelletta\Repositories\EloquentBarzellettaRepository;
use App\Plugins\Barzelletta\Repositories\FilesystemBarzellettaRepository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class RepositoryProviders extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (env('PLUGIN_DRIVER') == 'filesystem') {
            $this->app->bind(BarzellettaRepositoryInterface::class, function () {
                return new FilesystemBarzellettaRepository($this->app->make(Filesystem::class), env('PERSISTENCE_DIRECTORY'));
            });
        }

        if (env('PLUGIN_DRIVER') == 'eloquent') {
            $this->app->bind(BarzellettaRepositoryInterface::class, function () {
                return new EloquentBarzellettaRepository();
            });
        }

        if (!env('PLUGIN_DRIVER')) {
            throw new \InvalidArgumentException('A plugin driver must be set');
        }
    }
}
