<?php

namespace App\Providers;

use App\Plugins\Barzelletta\AddBarzelletta;
use App\Plugins\Barzelletta\Repositories\BarzellettaRepositoryInterface;
use App\Plugins\Barzelletta\SayBarzelletta;
use App\Plugins\PluginHandler;
use Illuminate\Support\ServiceProvider;

class PluginServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PluginHandler::class, function () {
            return new PluginHandler([
                new SayBarzelletta($this->app->make(BarzellettaRepositoryInterface::class)),
                new AddBarzelletta($this->app->make(BarzellettaRepositoryInterface::class))
            ]);
        });
    }
}
